#include "hit.hpp"

using namespace rt;

Hit::Hit(Ray g, vector p, vector n, color c) :
  gen(g), point(p), normal(n), col(c)
{}

Ray Hit::get_ray() const
{
  return gen;
}

vector Hit::get_point() const
{
  return point;
}

vector Hit::get_normal() const
{
  return normal;
}

color Hit::get_color() const
{
  return col;
}
