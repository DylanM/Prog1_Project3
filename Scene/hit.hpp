#ifndef HIT_HPP
#define HIT_HPP

#include "ray.hpp"

namespace rt
{
  class Hit
  {
  public:
    Hit(Ray g, vector p, vector n, color c);
    Ray get_ray() const;
    vector get_point() const;
    vector get_normal() const;
    color get_color() const;
  private:
    Ray gen;
    vector point;
    vector normal;
    color col;
  };
}

#endif // HIT_HPP
