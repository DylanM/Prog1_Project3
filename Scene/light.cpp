#include "light.hpp"
#include "hit.hpp"

using namespace rt;

Light::Light(const vector& p, const color& c) : pos(p), col(c) {}

vector Light::get_position() const {
    return pos;
}

color Light::get_color() const {
    return col;
}

color Light::apply(const Hit& hit) const {
  vector ray_dir = (pos - hit.get_point()).unit();
  double intensity = ray_dir | hit.get_normal();
  // Black if the intensity is negative ("eclipse")
  if (intensity < 0.)
    intensity = 0.;
  color hit_color = hit.get_color();
  unsigned red = (unsigned)(hit_color.get_red() * col.get_red()) / 255;
  unsigned green = (unsigned)(hit_color.get_green() * col.get_green()) / 255;
  unsigned blue = (unsigned)(hit_color.get_blue() * col.get_blue()) / 255;

  return color(intensity * red, intensity * green, intensity * blue);
}
