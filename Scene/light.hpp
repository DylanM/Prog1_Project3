#pragma once

#include "vector.hpp"
#include "color.hpp"
#include "hit.hpp"

namespace rt {
  class Light {
  private:
    vector pos;
    color col;
  public:
    Light(const vector& p, const color& c);
    vector get_position() const;
    color get_color() const;
    color apply(const Hit& r) const;
  };
}
