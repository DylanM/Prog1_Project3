#include "plane.hpp"
#include "color.hpp"
#include "vector.hpp"
#include "ray.hpp"
#include "hit.hpp"
#include <iostream>

using namespace rt;

Plane::Plane(const vector& _point, const vector& _normal, const color& _col):
    point(_point), normal(_normal.unit()), col(_col) {};

Plane::Plane(const vector& _point, const vector& _normal) :
    Plane(_point, _normal, color(193,193,193)) {};

double Plane::send(const Ray& r) const
{
  vector o = r.get_origin();
  vector d = r.get_direction();
    
  if (d.unit() == normal.unit() || d.unit() == (-1) * normal.unit())
    return infinity;
  else
    return ( (o - point) | normal ) / (d | normal);
}

Hit Plane::intersect(const Ray& r) const {
    double t = send(r);
    vector pt = r.get_origin() + t*r.get_direction();
    vector n = normal;
    return Hit(r, pt, n, col);
}
