#pragma once
#include "ray.hpp"
#include "hit.hpp"
#include "vector.hpp"
#include "object.hpp"
#include "color.hpp"
#include "core.hpp"

namespace rt
{
  class Plane : public Object
  {
  public:
    Plane(const vector& point, const vector& normal);
    Plane(const vector& point, const vector& normal, const color& col);
    inline vector get_point() const {return point;}
    inline vector get_normal() const {return normal;}
    inline color get_color() const {return col;}
    virtual double send(const Ray& r) const;
    virtual Hit intersect(const Ray& r) const;
  private:
      vector point;
      vector normal;
      color col;
  };
}
