#include "ray.hpp"

namespace rt
{

  Ray::Ray(vector o, vector d, color c) : orig(o), dir(d.unit()), col(c)
  {}
  
  Ray::Ray(vector o, vector d) : orig(o), dir(d.unit()), col(color::WHITE) {}

  vector Ray::get_origin() const {
    return orig;
  }

  vector Ray::get_direction() const {
    return dir;
  }

  color Ray::get_color() const {
    return col;
  }

}
