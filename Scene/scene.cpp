#include "scene.hpp"

using namespace rt;

Scene::Scene(std::vector<Object*>& o, const Camera& c, const vector& l) :
  Scene(o, c, Light(l, color::WHITE), color(200, 200, 200))
{}

Scene::Scene(std::vector<Object*>& o, const Camera& c,
	     const Light& l, const color& bg_col) :
  objects(o), camera(c), light_source(l), background_color(bg_col)
{}

// Look for the first object hit by a ray
Object* Scene::closest_hit_object(const Ray& ray)
{
  double minimal_distance = infinity;
  Object* closest_object = nullptr;

  // Iterate through all objects and find the closest hit
  for (std::vector<Object*>::iterator it = objects.begin();
       it != objects.end(); it++)
    {
      double distance = (*it)->send(ray);
      if (distance < minimal_distance)
	{
	  minimal_distance = distance;
	  closest_object = *it;
	}
    }
  return closest_object;
}
void Scene::test_collisions(screen& s) {
  // The image plane is normal to the camera's orientation
  // Center of the plane = camera pos + camera unit orientation vector
  // Tangent vectors : x direction (vertical) and camera orient^x (horizontal).
  // Example : camera orient = +z; z^x = +y

  vector vertical(1., 0., 0.);
  vector normal = camera.get_orientation();
  normal = vector(0., 0., 1.);
  vector horizontal = normal ^ vertical;
  horizontal = vector(0., 1., 0.);
  vector camera_pos = camera.get_position();
  
  // Origin of the screen plane
  vector screen_origin = camera_pos + camera_pos.norm() * normal
    - (s.width() / 2) * horizontal - (s.height() / 2) * vertical;
  for(unsigned i = 0; i <s.width(); ++i) {
    for(unsigned j = 0; j <s.height(); ++j) {
      vector screen_point = screen_origin + i * horizontal + j * vertical;
      Ray camera_through_screen(camera_pos, screen_point - camera_pos);
      //Look for the first object hit by this ray
      Object* closest_object = closest_hit_object(camera_through_screen);
      if(closest_object != nullptr) {
        s.set_pixel(i, j, color::WHITE);
      } else {
        s.set_pixel(i, j, color::BLACK);
      }
    }
  }
}
void Scene::render_scene(screen& s)
{
  // The image plane is normal to the camera's orientation
  // Center of the plane = camera pos + camera unit orientation vector
  // Tangent vectors : x direction (vertical) and camera orient^x (horizontal).
  // Example : camera orient = +z; z^x = +y

  vector vertical(1., 0., 0.);
  vector normal = camera.get_orientation();
  vector horizontal = normal ^ vertical;
  vector camera_pos = camera.get_position();
  
  // Origin of the screen plane
  vector screen_origin = camera_pos + camera_pos.norm() * normal
    - (s.width() / 2) * horizontal - (s.height() / 2) * vertical;
  
  // For each pixel, send a ray from the camera through this pixel
  for(unsigned i = 0; i < (unsigned)(s.width()); i++)
    {
      for(unsigned j = 0; j < (unsigned)(s.height()); j++)
	{
	  vector screen_point = screen_origin + i * horizontal + j * vertical;
	  Ray camera_screen_ray(camera_pos, screen_point - camera_pos);

	  // Look for the first object hit by this ray
	  Object* hit_object = closest_hit_object(camera_screen_ray);
	  if (hit_object != nullptr)
	    {
	      Hit hit = hit_object->intersect(camera_screen_ray);

	      // Send a ray from the hit point toward the light source
	      Ray hit_light_ray(hit.get_point(),
			       light_source.get_position() - hit.get_point());

	      // Check that no other object blocks this new ray
	      if (closest_hit_object(hit_light_ray) == nullptr
		  || closest_hit_object(hit_light_ray) == hit_object)

		// Fill the pixel with the color calculated by Light
		s.set_pixel(i, j, light_source.apply(hit));

	      else // The light is blocked, fill with the shade color
		s.set_pixel(i, j, color::BLACK);
	    }
	  else // The ray doesn't hit anything
	    s.set_pixel(i,j, background_color);
	}
    }
}
