#ifndef SCENE_HPP
#define SCENE_HPP

#include <vector>
#include "object.hpp"
#include "camera.hpp"
#include "screen.hpp"
#include "core.hpp"
#include "light.hpp"

namespace rt
{
  class Scene
  {
  public:
    Scene(std::vector<Object*>& o, const Camera& c, const vector& l);
    Scene(std::vector<Object*>& o, const Camera& c,
	  const Light& l, const color& bg_col);
    void render_scene(screen& s);
    Object* closest_hit_object(const Ray& ray);
    void test_collisions(screen& s);

  private:
    std::vector<Object*> objects;
    Camera camera;
    Light light_source;
    color background_color;
  };
}

#endif // SCENE_HPP
