#include "sphere.hpp"
#include "color.hpp"
#include "vector.hpp"
#include "ray.hpp"
#include "hit.hpp"
#include <cmath>

using namespace rt;

Sphere::Sphere(const vector& _c, double _r, const color& _col)
  :col(_col), radius(_r), center(_c)
{}

Sphere::Sphere(const vector& _c, double _r) : Sphere(_c, _r, color::WHITE) {}

double Sphere::send(const Ray& r) const{
  vector oc = r.get_origin() - center;
  double a = r.get_direction() | r.get_direction();
  double b = 2.0 * (oc | r.get_direction());
  double c = (oc | oc) - radius*radius;
  double delta = b*b - 4.0*a*c;
  if(delta > 0) {
    return (-b - sqrt(delta)) / (2.0*a);
    /*
// Y U NO WORK ?
//doesn't work
    double t_1 = (-b - sqrt(delta)) /(2.0*a);
    double t_2 = (-b + sqrt(delta)) /(2.0*a);
    if(t_2 >=0) {
      return(t_1 >=0)? t_1 : t_2;
    }else {
      return infinity;
    }
    */
  }else{
    return infinity;
  }
}

 
vector Sphere::get_intersect_point(double t, const vector& a, const vector& b) const {
  return vector(a.x + t*b.x, a.y + t*b.y, a.z + t*b.z);  
}

Hit Sphere::intersect(const Ray& r) const {
  vector ori = r.get_origin();
  vector u = r.get_direction();
  double t = send(r);
  vector i = get_intersect_point(t, ori, u);
  vector ci = (i - center);
  vector n = ci.unit();
  return Hit(r, i, n, col); 
}
