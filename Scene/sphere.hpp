#pragma once
#include "core.hpp"
#include "ray.hpp"
#include "hit.hpp"
#include "vector.hpp"
#include "object.hpp"
#include "color.hpp"

namespace rt
{
  class Sphere : public Object
  {
  public:
    Sphere(const vector& _c, double _r);
    Sphere(const vector& _c, double _r, const color& _col);
    inline vector get_center() const {return center;}
    inline double get_radius() const {return radius;}
    inline color get_color() const {return col;}
    virtual double send(const Ray& r) const;
    virtual Hit intersect(const Ray& r) const;
  private:
    vector get_intersect_point(double t, const vector& a, const vector& b) const;
    color col;
    double radius;
    vector center;
  };
}
