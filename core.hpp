#ifndef CORE_HPP
#define CORE_HPP

#include <limits>

const std::numeric_limits<double> real;
const double infinity = real.infinity();

#endif // CORE_HPP
