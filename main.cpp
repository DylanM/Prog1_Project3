#include <iostream>
#include <unistd.h>
#include <fstream>
#include <cassert>
#include <vector>
#include "image.hpp"
#include "screen.hpp"
#include "color.hpp"
#include "scene.hpp"
#include "sphere.hpp"
#include "plane.hpp"
#include "camera.hpp"
#include "vector.hpp"

using namespace rt;
int main() {

    rt::screen s(640,480);

    Sphere s1(rt::vector(0.0, 0.0, 0.0), 100.0, color::RED);
    s1.set_appearance(color::RED, 70.0, 30.0, 10.0);

    Plane p(rt::vector(400.0,40.0,40.0),rt::vector(-1.0, 0.0, 0.0), color::BLUE);
    p.set_appearance(color::BLUE, 100.0, 100.0, 1.0);

    std::vector<Object*> objects;
    objects.push_back(&p);
    //    objects.push_back(&s1);

    Camera camera(rt::vector(-50.0, 0.0, -1000.0), rt::vector(0.0, 0.0, 1.0));

    rt::Scene scene(objects, camera, rt::vector(-500., 0., -500.));

    scene.render_scene(s);
    s.update();

    // wait for the screen to be closed
    std::clog << "Waiting for QUIT event...";
    while(not s.wait_quit_event()) {}
    return 0;
}

 
